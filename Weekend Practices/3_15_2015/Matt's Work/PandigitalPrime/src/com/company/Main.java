package com.company;

import java.util.Scanner;

class Main {

    public static void main(String[] args) {
	    int numberDigits;
        PandigitalPrime pp = new PandigitalPrime();

        if((numberDigits = com.company.Main.acceptNumberOfDigits()) == -1) {
            System.out.println("Program Failed During Input");
        }
        else {
            System.out.println("numberDigits = " + numberDigits);
        }

        PandigitalPrime.runner(numberDigits);
    }

    public static int acceptNumberOfDigits() {
        int numDigit;
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter number of digits for pandigital number: ");

        try {
            numDigit = scanner.nextInt();
            return numDigit;
        }
        catch (Exception e) {
            System.out.println("IOError: " + e);
        }
        return -1;
    }

}

