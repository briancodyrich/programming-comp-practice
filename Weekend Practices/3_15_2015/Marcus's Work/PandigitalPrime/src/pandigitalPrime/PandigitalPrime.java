package pandigitalPrime;

import java.util.Arrays;

public class PandigitalPrime 
{
/***************this character array is used to determine if number is pandigital***************/
	 private static char[][] pandigits = new char[][]{
	    "1".toCharArray(),
	    "12".toCharArray(),
	    "123".toCharArray(),
	    "1234".toCharArray(),
	    "12345".toCharArray(),
	    "123456".toCharArray(),
	    "1234567".toCharArray(),
	    "12345678".toCharArray(),
	    "123456789".toCharArray(),
	};
	
	public static void main(String[] args) 
	{
		int count = 0;
		/*beginning at the maximum pandigital number (987654321),
		 * this iterates through all numbers greater than zero,
		 * decrementing by 2 each iteration (since even numbers clearly cannot be prime)
		 * brute force as fuck
		 */
		for(int i = 987654321; i > 0; i-=2)
		{
			if(isPandigital(i) && isPrime(i))
			{
				System.out.println(i);
				count++;
			}
		}
		System.out.println(count);
	}
	
	/***********determines if a number is prime******************/
	private static boolean isPrime(int num) 
	{
        if (num % 2 == 0) 
        	return false;
        for (int i = 3; i*i <= num; i += 2)
            if (num % i == 0) 
            	return false;
        return true;
	}
	
	/*************determines if a number is pandigital******************/
	private static boolean isPandigital(int i)
	{
		char[] c = String.valueOf(i).toCharArray();
		Arrays.sort(c);
		return Arrays.equals(c, pandigits[c.length-1]);
	}

}