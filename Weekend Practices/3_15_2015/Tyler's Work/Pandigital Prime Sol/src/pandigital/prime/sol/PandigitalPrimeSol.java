package pandigital.prime.sol;


public class PandigitalPrimeSol {
    

    private static final int MAX = 987654321;
    private static final int SQRT_MAX = (int) Math.sqrt(MAX) + 1;
    private static boolean[] array = new boolean[MAX];
    
    
    public static void main(String[] args) {
       
       
        for (int x = 1; x < SQRT_MAX; x++) {
            for (int y = 1; y < SQRT_MAX; y++) {
            int k = 4 * x * x + y * y;
            if ((k < MAX) && ((k % 12 == 1) || (k % 12 == 5))) {
              array[k] = !array[k];
            }
            k = 3 * x * x + y * y;
            if ((k < MAX) && (k % 12 == 7)) {
              array[k] = !array[k];
            }
            if (x > y) {
              k = 3 * x * x - y * y;
              if ((k < MAX) && (k % 12 == 11)) {
                array[k] = !array[k];
              }
            }
         }
        }
            array[2] = true;
            array[3] = true;
            for (int n = 5; n <= SQRT_MAX; n++) {
              if (array[n]) {
                int n2 = n * n;
                for (int k = n2; k < MAX; k += n2) {
                  array[k] = false;
                }
              }
            }
            
            
           checkPand(array, 987654321);
           checkPand(array, 98765432);
           checkPand(array, 9876543);
           checkPand(array, 987654);
           checkPand(array, 98765);
           checkPand(array, 9876);
           checkPand(array, 987);
           checkPand(array, 98);
           checkPand(array, 9);
           
    }
    
        public static boolean isPandigital(int x)
            {
                
                String str = Integer.toString(x);
                
                int count = 0;
                for (char i = '1'; i <= '9' ; i++)
                {
                    for (int j = 0; j < str.length(); j++)
                    {
                        if (i == str.charAt(j))
                        {
                            count++;
                        }
                        if (count > 1)
                        {
                            return false;
                        }
                    }
                    count = 0;
                }
                
                return true;
            }
    
    public static void checkPand(boolean[] arr, int x)
    {
         for (int i = x; i > 0; i--)
                {
                    if (arr[i-1] == true){
                        if (isPandigital(i-1) == true)
                        {
                            System.out.println(i-1);
                            return;
                        }
                }
        
        }
    }
}
