import java.awt.Point;
import java.util.ArrayList;


public final class MathExt 
{
	public static final double PHI = 1.61803399;
	public static final double OMPHI = 1 - PHI;
	public static final double PI = 3.14159265;
	public static long Factorial(int x)
	{
		if (x >= 1)
		{
			return x * Factorial(x-1);
		}
		else return 1;
	}
	public static long nCr(int n, int r)
	{
		return (Factorial(n) / (Factorial(r)*Factorial(n-r)));
	}
	public static long nPr(int n, int r)
	{
		return (Factorial(n) / (Factorial(n-r)));
	}
	public static double Distance(int x1, int y1, int x2, int y2)
	{
		return Math.sqrt(Math.pow(x2-x1, 2) + Math.pow(y2-y1, 2));
	}
	public static double Distance(Point p1, Point p2)
	{
		return Distance(p1.x, p1.y, p2.x, p2.y);
	}
	public static int GCD(int a, int b)
	{
		if (b > a)
		{
			return GCD(b,a);
		}
		if (a % b == 0)
		{
			return b;
		}
		else
		{
			return GCD(b, a%b);
		}
	}
	public static int LCM(int a, int b)
	{
		return Math.abs(a*b) / (GCD(a,b));
	}
	public static long Fib(int n)
	{
		return Math.round(((Math.pow(PHI, n) - Math.pow(OMPHI, n)) / Math.sqrt(5)));
	}
	public static double DegToRad(float theta)
	{
		return (theta * PI) / 180.0;
	}
	public static double[] PolarToRect(float r, float angle)
	{
		double[] ret = new double[2];
		ret[0] = r * Math.cos(angle);
		ret[1] = r * Math.sin(angle);
		return ret;
	}
	public static ArrayList<Integer> GetPrimes(int n)
	{
		int hold[] = new int[n-1];
		ArrayList<Integer> ret = new ArrayList<Integer>();
		for (int x = 0; x < hold.length; x++)
		{
			hold[x] = x+2;
		}
		int current = 0;
		while (current < hold.length)
		{
			if (hold[current] != 0)
			{
				ret.add(hold[current]);
				for (int y = current + hold[current]; y < hold.length; y += hold[current])
				{
					hold[y] = 0;
				}
				current++;
			}
			else
			{
				current++;
			}
			//System.out.println(current);
		}
		
		return ret;
	}

}
